# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170806090217) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bottom_adwords_urls", force: :cascade do |t|
    t.string "url"
    t.bigint "search_result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["search_result_id"], name: "index_bottom_adwords_urls_on_search_result_id"
  end

  create_table "non_adwords_urls", force: :cascade do |t|
    t.string "url"
    t.bigint "search_result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["search_result_id"], name: "index_non_adwords_urls_on_search_result_id"
  end

  create_table "reports", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "right_adwords_urls", force: :cascade do |t|
    t.string "url"
    t.bigint "search_result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["search_result_id"], name: "index_right_adwords_urls_on_search_result_id"
  end

  create_table "search_results", force: :cascade do |t|
    t.integer "top_adwords_count"
    t.integer "bottom_adwords_count"
    t.integer "total_adwords_count"
    t.integer "non_adwords_results_count"
    t.integer "total_links_count"
    t.string "total_of_search_results"
    t.text "html_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "report_id"
    t.string "keyword"
    t.index ["report_id"], name: "index_search_results_on_report_id"
  end

  create_table "top_adwords_urls", force: :cascade do |t|
    t.string "url"
    t.bigint "search_result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["search_result_id"], name: "index_top_adwords_urls_on_search_result_id"
  end

  add_foreign_key "bottom_adwords_urls", "search_results"
  add_foreign_key "non_adwords_urls", "search_results"
  add_foreign_key "right_adwords_urls", "search_results"
  add_foreign_key "search_results", "reports"
  add_foreign_key "top_adwords_urls", "search_results"
end
