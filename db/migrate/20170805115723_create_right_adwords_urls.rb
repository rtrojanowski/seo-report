class CreateRightAdwordsUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :right_adwords_urls do |t|
      t.string :url
      t.references :search_result, foreign_key: true

      t.timestamps
    end
  end
end
