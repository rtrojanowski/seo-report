class AddReportToSearchResults < ActiveRecord::Migration[5.1]
  def change
    add_reference :search_results, :report, foreign_key: true
  end
end
