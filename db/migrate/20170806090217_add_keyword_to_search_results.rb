class AddKeywordToSearchResults < ActiveRecord::Migration[5.1]
  def change
    add_column :search_results, :keyword, :string
  end
end
