class CreateSearchResults < ActiveRecord::Migration[5.1]
  def change
    create_table :search_results do |t|
      t.integer :top_adwords_count
      t.integer :bottom_adwords_count
      t.integer :total_adwords_count
      t.integer :non_adwords_results_count
      t.integer :total_links_count
      t.string :total_of_search_results
      t.text :html_code

      t.timestamps
    end
  end
end
