class GoogleScraperService
  class KeywordsLimitExceeded < StandardError; end
  include HttpSupport

  ASSOCIATIONS_KEYS = %i(top_adwords_urls bottom_adwords_urls non_adwords_urls right_adwords_urls).freeze

  def initialize(keywords)
    @keywords = keywords

    validate!
    search
  end

  def search
    report = Report.new

    @keywords.each do |keyword|
      query = keyword.gsub(' ', '+')
      page = HTTParty.get 'http://www.google.com/search',
        query: { q: query },
        headers: { "User-Agent" => HttpSupport::USER_AGENTS.sample }

      search_results = get_data_from(page)

      attributes = search_results
        .except(*ASSOCIATIONS_KEYS)
        .merge(keyword: keyword, report: report)
      urls_hash = search_results.slice(*ASSOCIATIONS_KEYS)

      record = SearchResult.new(attributes)
      record.build_urls(urls_hash)
      record.save!

      wait
    end
  end

  private

    def get_data_from(page)
      doc = Nokogiri::HTML(page)

      search_result = {
        top_adwords_count:         doc.css('div#tads li.ads-ad').size,
        bottom_adwords_count:      doc.css('div#bottomads li.ads-ad').size,
        total_adwords_count:       doc.css('li.ads-ad').size,
        non_adwords_results_count: doc.css('div#search div.g').size,
        top_adwords_urls:          doc.css('div#tads li.ads-ad div.ads-visurl cite').map(&:text),
        bottom_adwords_urls:       doc.css('div#bottomads li.ads-ad div.ads-visurl cite').map(&:text),
        non_adwords_urls:          doc.css('div#search div.g cite').map(&:text),
        total_links_count:         doc.css('a').size,
        total_of_search_results:   doc.css('div#resultStats').text,
        right_adwords_urls:        [], # TODO: remove it as google killed right hand side ads: https://searchenginewatch.com/2016/02/23/google-kills-right-hand-side-ads-what-does-this-mean-for-sem/
        html_code:                 page.parsed_response
      }
    end

    def validate!
      raise KeywordsLimitExceeded if @keywords.size > 1000
    end

    def wait
      sleep rand(20..30) unless Rails.env.test?
    end
end
