class WebScraperWorker
  include Sidekiq::Worker

  def perform(keywords)
    GoogleScraperService.new(keywords)
  end
end
