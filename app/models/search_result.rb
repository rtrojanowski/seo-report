class SearchResult < ApplicationRecord
  belongs_to :report

  has_many :top_adwords_urls
  has_many :bottom_adwords_urls
  has_many :non_adwords_urls
  has_many :right_adwords_urls #TODO it has been removed by Google

  scope :keyword_gteq, -> (query, n) { where("keyword similar to ?", "(%" + query + "%){" + n.to_s + ",}") }
  scope :with_urls, -> do
    self.left_outer_joins(:top_adwords_urls, :bottom_adwords_urls, :non_adwords_urls)
      .where("top_adwords_urls.search_result_id IS NOT NULL OR bottom_adwords_urls.search_result_id IS NOT NULL OR non_adwords_urls.search_result_id IS NOT NULL")
      .distinct
  end

  def build_urls(hash)
    hash[:top_adwords_urls].each do |url|
      self.top_adwords_urls << TopAdwordsUrl.new(url: url)
    end
    hash[:bottom_adwords_urls].each do |url|
      self.bottom_adwords_urls << BottomAdwordsUrl.new(url: url)
    end
    hash[:non_adwords_urls].each do |url|
      self.non_adwords_urls << NonAdwordsUrl.new(url: url)
    end
    hash[:right_adwords_urls].each do |url|
      self.right_adwords_urls << RightAdwordsUrls.new(url: url)
    end
  end
end
