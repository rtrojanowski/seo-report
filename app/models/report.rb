class Report < ApplicationRecord
  has_many :search_results, -> { order(keyword: 'asc') }

  scope :ordered, -> { order(created_at: 'desc') }

  def to_s
    "#{self.class.name} #{id} - #{created_at.strftime("%A, %B %d, %Y")}"
  end
end
