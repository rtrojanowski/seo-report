class RightAdwordsUrl < ApplicationRecord
  # TODO: remove it as google killed right hand side ads: https://searchenginewatch.com/2016/02/23/google-kills-right-hand-side-ads-what-does-this-mean-for-sem/
  belongs_to :search_result
end
