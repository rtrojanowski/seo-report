class SearchController < ApplicationController

  def index
    url_matches = params[:url_matches]
    url_eq = params[:url_eq]
    keyword_gteq_1 = params[:keyword_gteq_1]
    keyword_gteq_2 = params[:keyword_gteq_2]

    @url_matches_count = url_matches_count if url_matches.present?
    @url_eq_count = url_eq_count if url_eq.present?

    if keyword_gteq_2.present? || keyword_gteq_1.present?
      @keyword_gteq_2_count = keyword_gteq_2.present? ? keyword_gteq_2_scope.with_urls.count : 0
      @keyword_gteq_1_count = keyword_gteq_1.present? ? keyword_gteq_1_scope.with_urls.count : 0
      @keyword_gteq_total_count =  @keyword_gteq_2_count + @keyword_gteq_1_count
    end
  end

  private

    def url_matches_count
      TopAdwordsUrl.where('url like ?', "%#{params[:url_matches]}%").count +
      BottomAdwordsUrl.where('url like ?', "%#{params[:url_matches]}%").count
    end

    def url_eq_count
      NonAdwordsUrl.where('url like ?', "#{params[:url_eq]}").count +
      TopAdwordsUrl.where('url like ?', "%#{params[:url_eq]}%").count +
      BottomAdwordsUrl.where('url like ?', "%#{params[:url_eq]}%").count
    end

    def keyword_gteq_2_scope
      SearchResult.keyword_gteq(params[:keyword_gteq_2], 2)
    end

    def keyword_gteq_1_scope
      SearchResult.keyword_gteq(params[:keyword_gteq_1], 1)
    end
end
