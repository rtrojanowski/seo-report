require 'csv'

class UploadsController < ApplicationController

  def index; end

  def import
    keywords = CSV.read(params[:file].path).flatten.compact
    WebScraperWorker.perform_async(keywords)
    flash[:notice] = 'Processing in the background. Please refresh page in a few minutes.'
    redirect_to reports_url
  rescue
    flash[:error] = 'Problem with processing your file.'
    redirect_back fallback_location: root_url
  end
end
