class ReportsController < ApplicationController

  def index
    @reports = Report.ordered.paginate(page: params[:page], per_page: 20)
  end

  def show
    @report = Report
      .includes(search_results: [:top_adwords_urls, :bottom_adwords_urls, :non_adwords_urls, :right_adwords_urls])
      .find(params[:id])

    @search_results = @report.search_results.paginate(page: params[:page], per_page: 5)
  end
end
