require 'rails_helper'

describe SearchResult, type: :model do
  describe '#keyword_gteq scope' do
    let!(:search_result) { SearchResult.create!(
      keyword: ":/’sword /’s  word/’s word ’s",
      report: Report.new
    )}

    it 'returns proper scope' do
      expect(SearchResult.keyword_gteq("/’s", 2)).to contain_exactly(search_result)
      expect(SearchResult.keyword_gteq("/’s", 3)).to contain_exactly(search_result)
      expect(SearchResult.keyword_gteq("/’s", 4)).to be_empty
    end
  end

  describe '#with_urls scope' do
    let!(:search_result_a) { SearchResult.create!(
        keyword: "word",
        report: Report.new,
        non_adwords_urls: [NonAdwordsUrl.new, NonAdwordsUrl.new],
        top_adwords_urls: [TopAdwordsUrl.new],
        bottom_adwords_urls: [],
        right_adwords_urls: []
      )
    }

    let!(:search_result_b) {
      SearchResult.create!(
        keyword: "word",
        report: Report.new,
        non_adwords_urls: [],
        top_adwords_urls: [],
        bottom_adwords_urls: [],
        right_adwords_urls: []
      )
    }

    it 'returns proper scope' do
      expect(SearchResult.with_urls).to contain_exactly(search_result_a)
    end
  end
end
