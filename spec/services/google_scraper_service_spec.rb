require 'rails_helper'

describe GoogleScraperService do
  subject { described_class.new(keywords) }

  context 'when limit of keywords is exceeded' do
    let(:keywords) { Array.new(1001, "sample keyword") }

    it 'returns an errors' do
      expect { subject }.to raise_error(GoogleScraperService::KeywordsLimitExceeded)
    end
  end

  context "when result page does not have adwords" do
    let(:keywords) { ["car rental"] }
    subject { described_class.new(keywords) }

    it 'stores report with search resuts data' do
      VCR.use_cassette("google") do
        expect{ subject }.to change{ Report.count }.by(1).and \
                             change{ SearchResult.count }.by(1).and \
                             change { NonAdwordsUrl.count }.by(10)
      end
    end
  end

  context "when resuts page has adswords" do
    let(:keywords) { ["women's hats"] }

    subject { described_class.new(keywords) }

    it 'stores search results and associated data' do
      VCR.use_cassette("google_adwords") do
        expect{ subject }.to change{ SearchResult.count }.by(1).and \
                             change { TopAdwordsUrl.count }.by(1).and \
                             change { NonAdwordsUrl.count }.by(9)
      end
    end
  end
end
