Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :reports, only: [:index, :show]
  resources :uploads, only: [:index] do
    collection { post :import }
  end
  get 'search' => 'search#index'

  root :to => 'reports#index'
end
